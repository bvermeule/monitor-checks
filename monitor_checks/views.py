from django.conf import settings
from django.http import Http404, JsonResponse
from django.views.decorators.cache import never_cache
from django.views.decorators.http import require_GET

from monitor_checks import service


@never_cache
@require_GET
def endpoint(request):
    if not hasattr(settings, "MONITOR_SECRET") or not settings.MONITOR_SECRET:
        raise ValueError("Missing MONITOR_SECRET")

    if len(settings.MONITOR_SECRET) > 50:
        raise ValueError("MONITOR_SECRET is too long. Keep it at max 50 characters.")

    # Match Monitor-Secret header with setting
    if (
        "Monitor-Secret" not in request.headers
        or request.headers["Monitor-Secret"] != settings.MONITOR_SECRET
    ):
        raise Http404

    return JsonResponse(
        {"application_info": service.get_application_info(), "checks": service.get_checks()}
    )
