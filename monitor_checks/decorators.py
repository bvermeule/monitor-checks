CHECKS = []


def check(display_name: str, error_text: str):
    def decorator(func):
        name = func.__name__

        if len(name) > 20:
            raise ValueError(f"Function name {name} too long. Keep it at max 20 characters.")

        if len(display_name) > 20:
            raise ValueError(
                f"Display name {display_name} too long. Keep it at max 20 characters."
            )

        if len(display_name) > 255:
            raise ValueError(f"Error text too long. Keep it at max 255 characters.")

        CHECKS.append(
            {"name": name, "display_name": display_name, "error_text": error_text, "func": func}
        )

    return decorator
