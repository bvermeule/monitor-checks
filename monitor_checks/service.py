import platform

from monitor_checks.decorators import CHECKS


def _get_django_version():
    try:
        import django

        return django.get_version()
    except ImportError:
        return None


def _get_wagtail_version():
    try:
        from wagtail import __version__ as version

        return version
    except ImportError:
        return None


def _get_django_cms_version():
    try:
        from cms import __version__ as version

        return version
    except ImportError:
        return None


def get_application_info():
    return {
        "versions": {
            "python": platform.python_version(),
            "django": _get_django_version(),
            "django_cms": _get_django_cms_version(),
            "wagtail": _get_wagtail_version(),
        }
    }


def get_checks():
    results = []
    for check in CHECKS:
        value = check["func"]()

        result = {
            "name": check["name"],
            "display_name": check["display_name"],
            "error_text": check["error_text"],
            "is_ok": value,
        }

        results.append(result)
    return results
