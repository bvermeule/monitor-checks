import importlib

from django.apps import apps

for app in apps.get_app_configs():
    try:
        importlib.import_module(f"{app.name}.monitor_checks")
    except ModuleNotFoundError:
        # No monitor_checks.py
        pass
