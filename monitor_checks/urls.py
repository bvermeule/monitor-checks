from django.conf.urls import url

from monitor_checks import views

urlpatterns = [
    url(r"^$", views.endpoint),
]
