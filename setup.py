import setuptools

setuptools.setup(
    name="monitor-checks",
    version="0.1",
    description="Monitor companion package",
    long_description=open("README.md").read(),
    url="https://fabrique.nl",
    author="Fabrique",
    author_email="ontwikkelaars@fabrique.nl",
    packages=["monitor_checks"],
    install_requires=["Django>=2.2"]
)
