# Configure Django

## Add to urls.py
```
urlpatterns += [
    ...
    url(r"^checks/", include("monitor_checks.urls")),
    ...
]
```

### Add checks
Add a file named `monitor_checks.py` to your Django app and write your checks like this:
```
@check(display_name="My check", error_text="This does not look good.")
def my_check():
    <logic to determine whether check is OK (True) or not OK (False)> 
    return True
```

### Add settings

Use the following required settings:
```
MONITOR_SECRET = "my-key"
```


# Build new version of package

Update setuptools and wheel
`python3 -m pip install --upgrade setuptools wheel`
